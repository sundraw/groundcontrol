﻿using System;
using System.IO;

namespace GroundControl.Droid
{
    public class FileAccessHelper
    {
        public static string LocalFilePath(string filename)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string dbPath = Path.Combine(path, filename);

            return dbPath;
        }
    }
}
