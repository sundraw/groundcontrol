﻿using System;
using System.IO;

namespace GroundControl.iOS
{
    public class FileAccessHelper
    {
        public static string LocalFilePath(string filename)
        {
            string docDir = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string libDir = Path.Combine(docDir, "..", "Library", "Databases");

            if (!Directory.Exists(libDir))
            {
                Directory.CreateDirectory(libDir);
            }

            string dbPath = Path.Combine(libDir, filename);

            return dbPath;
        }
    }
}
