﻿using Xamarin.Forms;

namespace GroundControl
{
    public partial class App : Application
    {
        public const string DatabaseName = "data.sqlite";
        internal static string DatabasePath;

        public App(string dbPath)
        {
            DatabasePath = dbPath;

            InitializeComponent();

            MainPage = new NavigationPage(new GroundControlPage());
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
