﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Plugin.Connectivity;
using SQLite;
using Xamarin.Forms;

namespace GroundControl
{
    public partial class GroundControlPage : ContentPage
    {
        const string RequestUrl = "http://somatest.ground-control.co.uk/SummerMaintService/?q=dataRequest&token=4C68F84A-CFE1-4245-B782-300D60438CB11&parentID=&dataType=1005&dateTime=1900-01-01T00:00:00.000&latitude=0.0&longitude=0.0&radius=0.0&rangeStart=0&rangeEnd=50";

        public GroundControlPage()
        {
            InitializeComponent();
        }

#pragma warning disable RECS0165 // Asynchronous methods should return a Task instead of void
        protected override async void OnAppearing()
#pragma warning restore RECS0165 // Asynchronous methods should return a Task instead of void
        {
            base.OnAppearing();

            using (var db = new SQLiteConnection(App.DatabasePath))
            {
                db.CreateTable<Record>();

                List<Record> records = db.Table<Record>().ToList();
                if (records.Count > 0)
                {
                    recordsList.ItemsSource = records;
                }
                else
                {
                    if (CrossConnectivity.Current.IsConnected)
                    {
                        activityIndicator.IsRunning = true;
                        var response = await RequestData();
                        recordsList.ItemsSource = response;
                    }
                    else
                    {
                        await DisplayAlert("No connection", "There is no internet connection at the moment", "OK");
                    }
                }
            }
        }

        async Task<List<Record>> RequestData() 
        {
            using (var client = new HttpClient())
            {
                var jsonString = await client.GetStringAsync(RequestUrl);
                Response response = JsonConvert.DeserializeObject<Response>(jsonString);
                var records = response.Data.Records;
                using (var db = new SQLiteConnection(App.DatabasePath))
                {
                    db.DeleteAll<Record>();
                    foreach (Record record in records)
                    {
                        db.Insert(record);
                    }
                }

                activityIndicator.IsRunning = false;

                return records;
            }
        }
    }
}
