﻿using System;
namespace GroundControl
{
    public class Record
    {
        public string Id { get; set; }
        public string Client { get; set; }
        public string Site { get; set; }
    }
}
